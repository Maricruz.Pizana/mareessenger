import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  operation: string = 'login';
  email: string = null;
  password: string = null;
  nick: string = null;
  constructor(private authenticationService: AuthenticationService, private userService: UserService, private router: Router) { }

  ngOnInit(): void {
  }

  login() {
    this.authenticationService.loginWithEmail(this.email, this.password)
    .then( (data) => {
      const user = {
        uid: data.user.uid,
        email: this.email,
        nick: this.nick
      };
      this.userService.createUser(user).then((data2) => {
        alert('the login is correct');
        console.log(data);
        this.router.navigate(['home']);
      }).catch((error) => {
        alert('Warning');
        console.log(error);
      });
    }).catch((error) => {
      alert('Warning');
      console.log(error);
    });
  }

  register() {
    this.authenticationService.registerWithEmail(this.email, this.password)
    .then( (data) => {
      alert('The register is so correct');
      console.log(data);
    })
    .catch((error) => {
      alert('Warning');
      console.log(error);
    });
  }

}
