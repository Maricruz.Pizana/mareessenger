// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyC2uaLa13pfYkBxZ9NESRddRDk_t8Aq8VA",
    authDomain: "mareessenger.firebaseapp.com",
    databaseURL: "https://mareessenger.firebaseio.com",
    projectId: "mareessenger",
    storageBucket: "mareessenger.appspot.com",
    messagingSenderId: "703961088542",
    appId: "1:703961088542:web:caf2a4848bc0f55b84640f",
    measurementId: "G-20W69FK7NW"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
